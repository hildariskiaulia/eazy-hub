package com.example.eazyhub.data

import com.example.eazyhub.feature.model.OrderData
import com.example.eazyhub.data.source.remote.EazyApi.retrofitService
import com.example.eazyhub.data.source.local.UserPreferences
import com.example.eazyhub.ui.componen.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class EazyRespository(
    private val dataStore: UserPreferences,
    private val firebaseRef: DatabaseReference,
) {
    fun generateQrToFirebase(): Flow<String> = flow{
        val auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        if (user != null) {
            val newUser = User(author = user.uid, name = "Hilda Rizki Aulia")
            val postValues = newUser.toMap()

            val key = firebaseRef.push().key
            if (key != null) {
                firebaseRef.child(key).setValue(postValues)
            }
            emit( key ?: "")
        }
    }
    fun getUserIIdFromDatabase(qr:String, callback:(Pair<String,String>?) -> Unit){
        firebaseRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val userNode = dataSnapshot.child(qr)
                val userIdValue = userNode.child("userId").getValue(String::class.java)
                val tokenValue= userNode.child("userToken").getValue(String::class.java)
                val result = if (userIdValue != null && tokenValue != null ){
                    Pair(userIdValue,tokenValue)
                } else {
                    null
                }
                callback(result)
            }
            override fun onCancelled(error: DatabaseError) {
                callback(null)
            }
        })
    }

    suspend fun getUserData(userId: String, token: String): OrderData {
        return retrofitService.getUserData(userId, token)
    }

    suspend fun login(userId: String, token:String){
        dataStore.setUserId(userId)
        dataStore.setUserToken(token)
        dataStore.setUserLoggedIn(true)
    }
}

