package com.example.eazyhub.ui.componen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.NavigationBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.eazyhub.R
import com.example.eazyhub.ui.navigation.Screen
import com.example.eazyhub.ui.theme.EazyHubTheme


@Composable
fun Menu(
    navController: NavHostController,
    modifier: Modifier,
    onClick: (String) -> Unit,
    activemenu : String?
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Bottom,
    ) {
        Item(
            name = R.string.account,
            navController = navController,
            icon = R.drawable.account,
            route = Screen.Account,
            modifier = if (activemenu == "account") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier
        )
        Item(
            name = R.string.inbox,
            navController = navController,
            icon = R.drawable.inbox,
            route = Screen.Inbox,
            modifier = if (activemenu == "inbox") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier
        )

        Item(
            name = R.string.notifikasi,
            navController = navController,
            icon = R.drawable.notifikasi,
            route = Screen.Account,
            modifier = if (activemenu == "notifikasi") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier

        )
        Item(
            name = R.string.internet,
            navController = navController,
            icon = R.drawable.internet_connection,
            route = Screen.Account,
            modifier = if (activemenu == "internet connection") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier

        )
        Item(
            name = R.string.manage,
            navController = navController,
            icon = R.drawable.manage_app,
            route = Screen.Account,
            modifier = if (activemenu == "manage app") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier
        )
        Item(
            name = R.string.screen_size,
            navController = navController,
            icon = R.drawable.screen_size,
            route = Screen.Account,
            modifier = if (activemenu == "screen size") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier
        )
        Item(
            name = R.string.android_setting,
            navController = navController,
            icon = R.drawable.android_setting,
            route = Screen.Account,
            modifier = if (activemenu == "android setting") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier
        )
        Item(
            name = R.string.help,
            navController = navController,
            icon = R.drawable.help,
            route = Screen.Account,
            modifier = if (activemenu == "help") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier
        )
    }
}

@Preview(device = Devices.TV_1080p)
@Composable
fun MenuPreview() {
    val navController = rememberNavController()
    EazyHubTheme {
        Menu(navController = navController, modifier = Modifier, activemenu = null, onClick = { route ->
            navController.navigate(route)
        })
    }
}