package com.example.eazyhub.ui.componen

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.Text
import com.example.eazyhub.R
import com.example.eazyhub.ui.theme.EazyHubTheme

@OptIn(ExperimentalTvMaterial3Api::class, ExperimentalMaterial3Api::class)
@Composable
fun SearchBar (
    query: String,
    onQueryChange: (String) ->Unit,
    modifier: Modifier = Modifier
) {
    TextField(
        value = query,
        onValueChange = onQueryChange,
        leadingIcon = {
            Icon(painter = painterResource(id = R.drawable.search), contentDescription = "Search", tint = Color.White )
        },
        colors = TextFieldDefaults.textFieldColors(
            containerColor = Color(0x60000000),
            disabledIndicatorColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
        ),
        trailingIcon = {
            Icon(painter = painterResource(id = R.drawable.voice), contentDescription = null, tint = Color.White )
        },
        placeholder = {
            Text(text = stringResource(id = R.string.search) , color = Color.White)
        },
        modifier = modifier
            .padding(2.dp)
            .fillMaxWidth()
            .heightIn(min = 8.dp)
            .clip(RoundedCornerShape(100.dp))
    )
}

@Preview(device = Devices.TV_1080p)
@Composable
fun SearchBarPreview(){
    EazyHubTheme {
        SearchBar(query = "", onQueryChange = {})
    }
}