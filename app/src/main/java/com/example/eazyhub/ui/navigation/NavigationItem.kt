package com.example.eazyhub.ui.navigation

import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector

data class Item(
    val name: Int,
    val icon: Int,
    val screen: Screen,
    val modifier: Modifier
)
