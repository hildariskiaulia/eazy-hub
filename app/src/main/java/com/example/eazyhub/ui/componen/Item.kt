package com.example.eazyhub.ui.componen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.rememberNavController
import com.example.eazyhub.R
import com.example.eazyhub.ui.navigation.Screen
import com.example.eazyhub.ui.theme.EazyHubTheme

@Composable
fun Item(
    name: Int,
    icon: Int,
    route: Screen,
    modifier: Modifier,
    navController: NavController

){
    Button (
        onClick = {
                  navController.navigate(route.route) {
                      popUpTo(navController.graph.findStartDestination().id) {
                          saveState = true
                      }
                      restoreState = true
                      launchSingleTop = true
                  }
        },
        shape = RoundedCornerShape(50),
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Transparent
        ),
        modifier = modifier.padding( end = 110.dp)
    ){
        CompositionLocalProvider(LocalContentColor provides Color.Unspecified) {
            Icon(
                painter = painterResource(id = icon),
                contentDescription = null,
                tint = Color.Unspecified,
            )
            Text(
                text = stringResource(id = name),
                fontWeight = FontWeight.SemiBold,
                color = Color.White,
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(start = 10.dp)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ItemPreview(){
    EazyHubTheme {
        Item(
            name = R.string.account,
            icon = R.drawable.account,
            route = Screen.Account,
            modifier = Modifier.background(
                androidx.compose.ui.graphics.Color.Black
            ),
            navController = rememberNavController()
        )
    }
}

