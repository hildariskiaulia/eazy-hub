package com.example.eazyhub.ui.componen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.eazyhub.R
import com.example.eazyhub.ui.theme.EazyHubTheme

@Composable
fun TrailIcon(modifier: Modifier){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier.padding(12.dp)
    ){
        Icon(painter = painterResource
            (id = R.drawable.question),
            contentDescription = null,
            tint = Color.White,
            modifier = modifier.padding(12.dp)
            )
        Icon(painter = painterResource
            (id = R.drawable.inbox),
            contentDescription = null,
            tint = Color.White,
            modifier = modifier.padding(12.dp)
        )
        Icon(painter = painterResource
            (id = R.drawable.wifi2),
            contentDescription = null,
            tint = Color.White,
            modifier = modifier.padding(12.dp)
        )
        Text(
            text = stringResource(id = R.string.jam),
            color = Color.White,
            fontSize = 18.sp
        )
    }
}

@Preview(showBackground = true)
@Composable
fun TrailIconPreview(){
    EazyHubTheme {
        TrailIcon(modifier = Modifier.background(Color.Black))
    }
}