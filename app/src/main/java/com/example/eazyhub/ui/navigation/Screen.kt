package com.example.eazyhub.ui.navigation

sealed class Screen(val route: String) {
    object Login : Screen("login")

    object Account : Screen("account")
    object Inbox : Screen("inbox")

    object Notifikasi : Screen("notifikasi")

    object InternetConnection : Screen("internet connection")

    object ManageApp : Screen("manage app")

    object ScreenSize : Screen("screen size")

    object AndroidSetting : Screen("android setting")

    object Help : Screen("help")

}