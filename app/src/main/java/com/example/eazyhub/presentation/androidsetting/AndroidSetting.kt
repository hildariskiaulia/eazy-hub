package com.example.eazyhub.presentation.androidsetting

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.example.eazyhub.R
import com.example.eazyhub.ui.theme.EazyHubTheme

@Composable
fun AndroidSetting(
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier.fillMaxHeight()
    ) {
        androidx.compose.material3.Text(
            text = "Android Setting",
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)
        )
        Column(
            verticalArrangement = Arrangement.spacedBy(20.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = modifier
                .requiredWidth(width = 500.dp)
                .clip(shape = RoundedCornerShape(10.dp))
                .background(color = Color.Black.copy(alpha = 0.5f))
                .padding(vertical = 40.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.android_settings_menu),
                contentDescription = null,
                modifier = Modifier
                    .requiredWidth(width = 190.dp)
                    .requiredHeight(height = 210.dp)
            )
            Column(
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .requiredWidth(width = 311.dp)
                    .clip(shape = RoundedCornerShape(100.dp))
                    .background(color = Color(0xff2970ff))
                    .padding(horizontal = 20.dp, vertical = 12.dp)
            ) {
                androidx.compose.material3.Text(
                    text = "Open Device Setting",
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    lineHeight = 1.5.em,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold
                    ),
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
    }
}


@Preview(device = Devices.TV_720p)
@Composable
fun AccountScreenPreview() {
    EazyHubTheme {
        AndroidSetting()
    }
}