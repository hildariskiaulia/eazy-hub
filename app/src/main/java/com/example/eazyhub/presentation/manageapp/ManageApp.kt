package com.example.eazyhub.presentation.manageapp

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.BlurredEdgeTreatment
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.tv.material3.Text
import com.example.eazyhub.R
import com.example.eazyhub.ui.theme.EazyHubTheme

@Composable
fun ManageApp(
    modifier: Modifier = Modifier,
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(5.dp, Alignment.Top),
        modifier = modifier.fillMaxSize()
    ) {
        androidx.compose.material3.Text(
            text = "Manage App",
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)
        )
        LazyRow(
            horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .requiredWidth(width = 556.dp)
                .clip(shape = RoundedCornerShape(8.dp))
                .background(color = Color.Black.copy(alpha = 0.5f))
                .padding(all = 16.dp)
        ) {
            item {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.requiredWidth(width = 255.dp)
                ) {
                    Box(
                        modifier = Modifier
                            .requiredSize(size = 60.dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .clip(shape = CircleShape)
                                .background(color = Color(0xffe3fcef))
                        )
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .clip(shape = CircleShape)
                                .background(color = Color(0xff36b37e))
                        )
                        androidx.compose.material3.Text(
                            text = "50%",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold
                            ),
                            modifier = Modifier
                                .align(alignment = Alignment.TopStart)
                                .offset(x = 12.dp, y = 18.dp)
                        )
                    }
                    Column(
                        verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.Top)
                    ) {
                        androidx.compose.material3.Text(
                            text = "8.00 GB / 16.00 GB",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold
                            )
                        )
                        androidx.compose.material3.Text(
                            text = "Memory Used",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 14.sp
                            )
                        )
                    }
                }
            }
        }
        Spacer(modifier = Modifier.height(16.dp))
        Column(
            verticalArrangement = Arrangement.spacedBy(15.dp, Alignment.Top),
            modifier = Modifier.requiredWidth(width = 556.dp)
        ) {
            androidx.compose.material3.Text(
                text = "Applications List",
                color = Color.White,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold
                ),
            )
            Column(
                verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
                modifier = Modifier.fillMaxWidth()
            ) {
                LazyRow(
                    horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clip(shape = RoundedCornerShape(8.dp))
                        .background(color = Color.Black.copy(alpha = 0.5f))
                        .padding(all = 16.dp)
                ) {
                    item {
                        Box(
                            modifier = Modifier
                                .requiredWidth(width = 120.dp)
                                .requiredHeight(height = 60.dp)
                        ) {
                            Box(
                                modifier = Modifier
                                    .requiredWidth(width = 120.dp)
                                    .requiredHeight(height = 60.dp)
                                    .clip(shape = RoundedCornerShape(10.dp))
                                    .background(color = Color(0xfffe0002))
                            )
                            Image(
                                painter = painterResource(id = R.drawable.youtube),
                                contentDescription = null,
                                modifier = Modifier
                                    .align(alignment = Alignment.TopStart)
                                    .offset(x = 40.dp, y = 10.dp)
                                    .requiredSize(size = 40.dp)
                            )
                        }
                    }
                    item {
                        androidx.compose.material3.Text(
                            text = "Youtube",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )
                    }
                    item {
                        androidx.compose.material3.Text(
                            text = "                              217.7 MB  |  12/03/2022",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 15.sp,
                            ),
                        )
                    }
                }
            }
        }
    }
}

@Preview(device = Devices.TV_720p)
@Composable
fun ManageAppPreview() {
    EazyHubTheme {
        ManageApp()
    }
}