package com.example.eazyhub.presentation.login

import androidx.lifecycle.ViewModel
import com.example.eazyhub.data.EazyRespository
import kotlinx.coroutines.flow.Flow

class LoginViewModel(
    private val repository: EazyRespository,
) : ViewModel(){
    fun generateQRToFirebase(): Flow<String> {
        return repository.generateQrToFirebase()
    }
}