package com.example.eazyhub.presentation

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.eazyhub.R
import com.example.eazyhub.presentation.account.AccountScreen
import com.example.eazyhub.presentation.login.LoginScreen
import com.example.eazyhub.data.source.local.UserPreferences
import com.example.eazyhub.presentation.androidsetting.AndroidSetting
import com.example.eazyhub.presentation.help.Help
import com.example.eazyhub.presentation.inbox.Inbox
import com.example.eazyhub.presentation.internetconnection.InternetConnection
import com.example.eazyhub.presentation.manageapp.ManageApp
import com.example.eazyhub.presentation.notifikasi.Notifikasi
import com.example.eazyhub.presentation.screensize.ScreenSize
import com.example.eazyhub.ui.componen.Menu
import com.example.eazyhub.ui.componen.PopupLoginQR
import com.example.eazyhub.ui.componen.SearchBar
import com.example.eazyhub.ui.componen.TrailIcon
import com.example.eazyhub.ui.navigation.Screen
import com.example.eazyhub.ui.theme.EazyHubTheme


@Composable
fun EazyHubApp(modifier: Modifier = Modifier) {
    val navController = rememberNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val activemenu = navBackStackEntry?.destination?.route
    var showPopup by remember { mutableStateOf(false) }
    var qrValue by remember { mutableStateOf("xxx") }

    val userPreferences = UserPreferences(LocalContext.current)
    val isLoggedIn by userPreferences.isUserLoggedIn.collectAsState(initial = false)

    val startDestination = if (isLoggedIn){
        Screen.Account.route
    } else {
        Screen.Login.route
    }


    Box(
        modifier = modifier
            .fillMaxSize()
            .background(Color.White),
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.background),
            contentDescription = null,
            contentScale = ContentScale.FillWidth,
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Transparent)
        )

        Column(modifier = modifier) {
            Row(
                modifier = Modifier.padding(start = 24.dp, top = 4.dp, bottom = 4.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Row(
                    modifier = Modifier.width(560.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    IconButton(
                        onClick = {},
                        modifier = Modifier
                            .padding(0.dp)
                            .background(Color(0x70000000), shape = CircleShape)
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.navbar),
                            contentDescription = null,
                            tint = Color.White
                        )
                    }
                    IconButton(
                        onClick = {},
                        modifier = Modifier
                            .padding(10.dp)
                            .background(Color(0x70000000), shape = CircleShape)
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.lonceng),
                            contentDescription = null,
                            tint = Color.White
                        )
                    }
                    SearchBar(
                        query = "",
                        onQueryChange = {},
                        modifier = Modifier.padding(start = 24.dp)
                    )
                }
                Spacer(modifier = Modifier.weight(1f))
                TrailIcon(modifier = modifier)
            }
            Row(modifier = modifier.padding(top = 20.dp)) {
                Column(modifier = modifier) {
                    Menu(
                        navController = navController,
                        modifier = modifier.padding(start = 16.dp),
                        onClick = { route ->
                            navController.navigate(route)
                        }, activemenu = String()
                    )
                }
                NavHost(
                    navController = navController,
                    startDestination = if (isLoggedIn)Screen.Account.route else Screen.Login.route
                ) {
                    composable(Screen.Login.route) {
                        LoginScreen(modifier = modifier, { showPopup = true }, onQrGenerate={qrValue=it})
                    }
                    composable(Screen.Account.route) {
                        AccountScreen(modifier = modifier, qrValue=qrValue)
                    }
                    composable(Screen.Inbox.route) {
                        Inbox(modifier = modifier)
                    }
                    composable(Screen.Notifikasi.route) {
                        Notifikasi(modifier = modifier)
                    }
                    composable(Screen.InternetConnection.route) {
                        InternetConnection(modifier = modifier)
                    }
                    composable(Screen.ManageApp.route) {
                       ManageApp(modifier = modifier)
                    }
                    composable(Screen.ScreenSize.route) {
                       ScreenSize(modifier = modifier)
                    }
                    composable(Screen.AndroidSetting.route) {
                        AndroidSetting(modifier = modifier)
                    }
                    composable(Screen.Help.route) {
                        Help(modifier = modifier)
                    }
                }
            }
        }
        if (showPopup) {
            PopupLoginQR(
                modifier = Modifier.width(700.dp),
                onClosePopup = { showPopup = false },
                onLoginSuccess = { navController.navigate("account") },
                qrValue = qrValue
            )
        }
    }
}

@Preview(showBackground = true, device = Devices.TV_720p)
@Composable
fun EazyHubAppPreview() {
    EazyHubTheme {
        EazyHubApp()
    }
}
