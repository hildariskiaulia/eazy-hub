package com.example.eazyhub.presentation.internetconnection

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.BlurredEdgeTreatment
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.tv.material3.Text
import com.example.eazyhub.R
import com.example.eazyhub.ui.theme.EazyHubTheme

@Composable
fun InternetConnection(
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier.fillMaxHeight()
    ) {
        androidx.compose.material3.Text(
            text = "Internet Connection",
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)
        )
        Column(
            verticalArrangement = Arrangement.spacedBy(20.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = modifier
                .requiredWidth(width = 500.dp)
                .clip(shape = RoundedCornerShape(10.dp))
                .background(color = Color.Black.copy(alpha = 0.5f))
                .padding(vertical = 40.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.wifi2),
                contentDescription = null,
                modifier = Modifier
                    .requiredWidth(width = 120.dp)
                    .requiredHeight(height = 60.dp)
            )
            Column(
                verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                androidx.compose.material3.Text(
                    text = "You are connected",
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    lineHeight = 1.5.em,
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
                Row(
                    horizontalArrangement = Arrangement.spacedBy(
                        12.dp,
                        Alignment.CenterHorizontally
                    )
                ) {
                    androidx.compose.material3.Text(
                        text = "Access Type",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(fontSize = 16.sp),
                        modifier = Modifier.requiredWidth(width = 120.dp)
                    )
                    androidx.compose.material3.Text(
                        text = ": Wifi Network",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(fontSize = 16.sp),
                        modifier = Modifier.requiredWidth(width = 110.dp)
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.spacedBy(
                        12.dp,
                        Alignment.CenterHorizontally
                    ),
                    modifier = Modifier.requiredWidth(width = 240.dp)
                ) {
                    androidx.compose.material3.Text(
                        text = "Connection",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(fontSize = 16.sp),
                        modifier = Modifier.requiredWidth(width = 120.dp)
                    )
                    androidx.compose.material3.Text(
                        text = ": Mi-FadsSys",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(fontSize = 16.sp),
                        modifier = Modifier.requiredWidth(width = 110.dp)
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.spacedBy(
                        12.dp,
                        Alignment.CenterHorizontally
                    )
                ) {
                    androidx.compose.material3.Text(
                        text = "Status",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(fontSize = 16.sp),
                        modifier = Modifier.requiredWidth(width = 120.dp)
                    )
                    androidx.compose.material3.Text(
                        text = ": Connected",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(fontSize = 16.sp),
                        modifier = Modifier.requiredWidth(width = 110.dp)
                    )
                }
            }
            Column(
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .requiredWidth(width = 311.dp)
                    .clip(shape = RoundedCornerShape(100.dp))
                    .background(color = Color(0xff2970ff))
                    .padding(horizontal = 20.dp, vertical = 12.dp)
            ) {
                androidx.compose.material3.Text(
                    text = "Edit Configuration",
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    lineHeight = 1.5.em,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold
                    ),
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
    }
}


@Preview(device = Devices.TV_720p)
@Composable
fun AccountScreenPreview() {
    EazyHubTheme {
        InternetConnection()
    }
}