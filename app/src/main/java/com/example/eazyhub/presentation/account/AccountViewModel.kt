package com.example.eazyhub.presentation.account

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eazyhub.feature.model.OrderData
import com.example.eazyhub.data.source.remote.UserDataResponse
import com.example.eazyhub.data.EazyRespository
import com.example.eazyhub.data.source.local.UserPreferences
import com.example.eazyhub.feature.model.util.UiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch


class AccountViewModel(
    private val repository: EazyRespository,
    private val userPreferences: UserPreferences
) : ViewModel() {
    private val _uiState: MutableStateFlow<UiState<OrderData>> =
        MutableStateFlow(UiState.Loading)
    val uiState: StateFlow<UiState<OrderData>>
        get() = _uiState


    private val _state = MutableStateFlow(
        UserDataResponse(
            userId = "",
            fullname = "",
            email = "",
            profile = ""
        )
    )
    val state = _state.asStateFlow()

    private val userToken: Flow<String?> = userPreferences.userToken
    private val userId: Flow<String?> = userPreferences.userId
    init {
        getUserData()
    }
    fun getUserData() {
        viewModelScope.launch {
            val userId = userId.firstOrNull()
            val token = userToken.firstOrNull()

            if (!userId.isNullOrBlank() && !token.isNullOrBlank()) {
                val userData = repository.getUserData(userId, "Bearer $token")
                Log.d("Hilda", userData.data.toString())
                _state.value = (userData.data.get(0))
            }
        }
    }

    fun SignOut() {
        viewModelScope.launch(Dispatchers.IO) {
            userPreferences.clearUserData()
        }
    }
}