package com.example.eazyhub.feature.model

import com.example.eazyhub.data.source.remote.UserDataResponse
import kotlinx.serialization.Serializable

@Serializable
data class OrderData(
    val data: List <UserDataResponse>
)